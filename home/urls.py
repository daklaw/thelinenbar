from django.conf.urls import patterns, include, url

urlpatterns = patterns(
    'home.views',
    url(r'^$', 'home', name='home'),
    url(r'^linens/$', 'linens', name='linens'),
    url(r'^contact/$', 'contact', name='contact'),
    url(r'^blog/', 'blog', name='blog'),
)