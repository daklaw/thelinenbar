from django.shortcuts import render

# Create your views here.
def home(request):
    return render(request, 'home/index.html')

def linens(request):
    return render(request, 'home/linens.html')

def contact(request):
    return render(request, 'home/contact.html')

def blog(request):
    return render(request, 'home/blog.html')